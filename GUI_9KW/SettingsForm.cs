﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace GUI_9KW
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
           
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {   
            comboBox1.Text = Properties.Settings.Default.Zoom.ToString();

            textBox2.Text = Properties.Settings.Default.WaveSoundFolder;
            textBox1.Text = Properties.Settings.Default.apikey;
            checkBox1.Checked = Properties.Settings.Default.UseWindowsSound;
            checkBox2.Checked = Properties.Settings.Default.UseWaveSound;
            checkBox3.Checked = Properties.Settings.Default.UseInternalSound;
            checkBox10.Checked = Properties.Settings.Default.NoSound;
            checkBox4.Checked = Properties.Settings.Default.AutoMinimize;
            checkBox5.Checked = Properties.Settings.Default.ActivateWindow;
            checkBox6.Checked = Properties.Settings.Default.AutoStart;
            checkBox7.Checked = Properties.Settings.Default.ZoomManually;
            checkBox8.Checked = Properties.Settings.Default.KeepAR;
            checkBox9.Checked = Properties.Settings.Default.HideElements;
            checkBoxGrayscale.Checked = Properties.Settings.Default.Grayscale;
            checkBoxProxy.Checked = Properties.Settings.Default.Proxy;
            checkBox14.Checked = Properties.Settings.Default.Speed;
            checkBox13.Checked = Properties.Settings.Default.Confirm;
            checkBox12.Checked = Properties.Settings.Default.Mouse;
            checkBox11.Checked = Properties.Settings.Default.Text;
            cb_setting_debug.Checked = Properties.Settings.Default.debug;
            

            if (Properties.Settings.Default.apikey == "")
            {
                System.Windows.Forms.MessageBox.Show("No API Key found!\r\nPlease enter an API Key");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.UseWindowsSound = checkBox1.Checked;
            Properties.Settings.Default.UseWaveSound = checkBox2.Checked;
            Properties.Settings.Default.UseInternalSound = checkBox3.Checked;
            Properties.Settings.Default.NoSound = checkBox10.Checked;
            Properties.Settings.Default.AutoMinimize = checkBox4.Checked;
            Properties.Settings.Default.ActivateWindow = checkBox5.Checked;
            Properties.Settings.Default.AutoStart = checkBox6.Checked;
            Properties.Settings.Default.ZoomManually = checkBox7.Checked;
            Properties.Settings.Default.Zoom = Convert.ToInt32(comboBox1.Text);
            Properties.Settings.Default.KeepAR = checkBox8.Checked;
            Properties.Settings.Default.HideElements = checkBox9.Checked;
            Properties.Settings.Default.Text = checkBox11.Checked;
            Properties.Settings.Default.Mouse = checkBox12.Checked;
            Properties.Settings.Default.Confirm = checkBox13.Checked;
            Properties.Settings.Default.Speed = checkBox14.Checked;
            Properties.Settings.Default.Proxy = checkBoxProxy.Checked;
            Properties.Settings.Default.Grayscale = checkBoxGrayscale.Checked;
            Properties.Settings.Default.WaveSoundFolder = textBox2.Text;
            Properties.Settings.Default.debug = cb_setting_debug.Checked;

            if (textBox1.Text != "")
            {
                try
                {
                    String rqurl = "http://www.9kw.eu/index.cgi?action=userservercheck&apikey=" + textBox1.Text;
                    WebRequest wrequest;
                    wrequest = WebRequest.Create(rqurl);
                    Stream responsestream = wrequest.GetResponse().GetResponseStream();
                    StreamReader streamreader = new StreamReader(responsestream, Encoding.UTF8);
                    String result = streamreader.ReadToEnd();
                    streamreader.Close();
                    responsestream.Close();
                    if (result.Contains("worker"))
                    {
                        Properties.Settings.Default.apikey = textBox1.Text;
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("API Key not valid! \r\nError:" + result);

                    }
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");
                }
            }
            else 
            {
                Properties.Settings.Default.apikey = textBox1.Text;
                System.Windows.Forms.MessageBox.Show("No API Key found!\r\nProgramm won't work!");
            }
            
            Properties.Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                checkBox3.Checked = false;
                checkBox2.Checked = false;
                
            }
            
        }

        private void checkBox2_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {

                checkBox1.Checked = false;
                checkBox3.Checked = false;
                
                if(textBox2.Text == "")
                {
                    openSoundFile.ShowDialog();
                }


                if (textBox2.Text == "" | textBox2.Text == "Cancel")
                {
                    
                    checkBox2.Checked = false;

                    textBox2.Text = "";
                }
            }
            else 
            {
                textBox2.Text = "";   
            }
            
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                
            }
            
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox7.Checked)
            {   
                
                comboBox1.Text = "100";
                comboBox1.Enabled = false;
                checkBox8.Enabled = true;
            }
            else
            {
                comboBox1.Enabled = true;
                checkBox8.Checked = false;
                checkBox8.Enabled = false;
            }
               
            
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                checkBox5.Enabled = false;
            }
            else
            {
                checkBox5.Enabled = true;
            }
        }

        private void openSoundFile_FileOk(object sender, CancelEventArgs e)
        {
            textBox2.Text = openSoundFile.FileName;
        }
    }
}
