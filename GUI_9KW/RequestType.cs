﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUI_9KW
{
    public class RequestType
    {
        private string source = "&source=9kwgui";
        public string captchaStatus { get; set; }
        public string serverStatus { get; set; }
        public string captchaID { get; set; }
        public string captcha { get; set; }
        public string skipCaptcha { get; set; }
        public string showCaptcha { get; set; }

        public RequestType()
        {
            this.captchaStatus = "action=usercaptchaguthaben" + source;
            this.serverStatus = "action=userservercheck" + source;
            this.captchaID = "action=usercaptchanew&nocaptcha=1&withok=1" + source;
            this.captcha = "action=usercaptchacorrect" + source;
            this.skipCaptcha = "action=usercaptchaskip" + source;
            this.showCaptcha = "action=usercaptchashow" + source;
        }
    }
}
