﻿namespace GUI_9KW
{
    partial class FrmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.lblCredits = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnSkip = new System.Windows.Forms.Button();
            this.menuTop = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbCaptchaImage = new System.Windows.Forms.PictureBox();
            this.txtCaptcha = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblCounter = new System.Windows.Forms.Label();
            this.sttInfo = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblInQueue = new System.Windows.Forms.Label();
            this.lblnWork = new System.Windows.Forms.Label();
            this.bgwGetCaptchaID = new System.ComponentModel.BackgroundWorker();
            this.bgwUpdateCredits = new System.ComponentModel.BackgroundWorker();
            this.bgwUpdateServerStatus = new System.ComponentModel.BackgroundWorker();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1true = new System.Windows.Forms.Button();
            this.button2false = new System.Windows.Forms.Button();
            this.menuTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaptchaImage)).BeginInit();
            this.sttInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Credits:";
            // 
            // lblCredits
            // 
            this.lblCredits.AutoSize = true;
            this.lblCredits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredits.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCredits.Location = new System.Drawing.Point(67, 12);
            this.lblCredits.Name = "lblCredits";
            this.lblCredits.Size = new System.Drawing.Size(13, 13);
            this.lblCredits.TabIndex = 1;
            this.lblCredits.Text = "0";
            // 
            // btnStart
            // 
            this.btnStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnStart.Location = new System.Drawing.Point(21, 62);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSkip
            // 
            this.btnSkip.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSkip.Location = new System.Drawing.Point(199, 62);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(75, 23);
            this.btnSkip.TabIndex = 4;
            this.btnSkip.Text = "Skip";
            this.btnSkip.UseVisualStyleBackColor = true;
            this.btnSkip.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuTop
            // 
            this.menuTop.AutoSize = false;
            this.menuTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuTop.Location = new System.Drawing.Point(0, 0);
            this.menuTop.Name = "menuTop";
            this.menuTop.Size = new System.Drawing.Size(300, 24);
            this.menuTop.TabIndex = 5;
            this.menuTop.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem1.Text = "?";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // pbCaptchaImage
            // 
            this.pbCaptchaImage.BackColor = System.Drawing.SystemColors.Control;
            this.pbCaptchaImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbCaptchaImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbCaptchaImage.ImageLocation = "";
            this.pbCaptchaImage.Location = new System.Drawing.Point(0, 124);
            this.pbCaptchaImage.MinimumSize = new System.Drawing.Size(300, 60);
            this.pbCaptchaImage.Name = "pbCaptchaImage";
            this.pbCaptchaImage.Size = new System.Drawing.Size(300, 60);
            this.pbCaptchaImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCaptchaImage.TabIndex = 6;
            this.pbCaptchaImage.TabStop = false;
            this.pbCaptchaImage.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pbCaptchaImage.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pbCaptchaImage.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // txtCaptcha
            // 
            this.txtCaptcha.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtCaptcha.Location = new System.Drawing.Point(0, 184);
            this.txtCaptcha.Name = "txtCaptcha";
            this.txtCaptcha.Size = new System.Drawing.Size(300, 20);
            this.txtCaptcha.TabIndex = 7;
            this.txtCaptcha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCounter.ForeColor = System.Drawing.Color.Red;
            this.lblCounter.Location = new System.Drawing.Point(242, 15);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(46, 31);
            this.lblCounter.TabIndex = 8;
            this.lblCounter.Text = "29";
            // 
            // sttInfo
            // 
            this.sttInfo.AutoSize = false;
            this.sttInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.sttInfo.Location = new System.Drawing.Point(0, 204);
            this.sttInfo.Name = "sttInfo";
            this.sttInfo.Size = new System.Drawing.Size(300, 23);
            this.sttInfo.SizingGrip = false;
            this.sttInfo.TabIndex = 9;
            this.sttInfo.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 18);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "User:";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(67, 29);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(13, 13);
            this.lblUser.TabIndex = 11;
            this.lblUser.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "In Work:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(136, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "In Queue:";
            // 
            // lblInQueue
            // 
            this.lblInQueue.AutoSize = true;
            this.lblInQueue.Location = new System.Drawing.Point(196, 30);
            this.lblInQueue.Name = "lblInQueue";
            this.lblInQueue.Size = new System.Drawing.Size(13, 13);
            this.lblInQueue.TabIndex = 14;
            this.lblInQueue.Text = "0";
            // 
            // lblnWork
            // 
            this.lblnWork.AutoSize = true;
            this.lblnWork.Location = new System.Drawing.Point(196, 12);
            this.lblnWork.Name = "lblnWork";
            this.lblnWork.Size = new System.Drawing.Size(13, 13);
            this.lblnWork.TabIndex = 15;
            this.lblnWork.Text = "0";
            // 
            // bgwGetCaptchaID
            // 
            this.bgwGetCaptchaID.WorkerSupportsCancellation = true;
            this.bgwGetCaptchaID.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGetCaptchaID_DoWork);
            this.bgwGetCaptchaID.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwGetCaptchaID_RunWorkerCompleted);
            // 
            // bgwUpdateCredits
            // 
            this.bgwUpdateCredits.WorkerSupportsCancellation = true;
            this.bgwUpdateCredits.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwUpdateCredits_DoWork);
            // 
            // bgwUpdateServerStatus
            // 
            this.bgwUpdateServerStatus.WorkerSupportsCancellation = true;
            this.bgwUpdateServerStatus.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwUpdateServerStatus_DoWork);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(23, 23);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnSkip);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblCredits);
            this.panel1.Controls.Add(this.lblnWork);
            this.panel1.Controls.Add(this.lblCounter);
            this.panel1.Controls.Add(this.lblInQueue);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lblUser);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 100);
            this.panel1.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(253, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "SOLUTION: ";
            this.label2.Visible = false;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.menuTray;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "GUI_9KW";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // menuTray
            // 
            this.menuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem2,
            this.toolStripMenuItem4,
            this.toolStripMenuItem6,
            this.toolStripMenuItem5});
            this.menuTray.Name = "menuTray";
            this.menuTray.Size = new System.Drawing.Size(122, 114);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem3.Text = "Exit";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem2.Text = "Hide GUI";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem4.Text = "Settings";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem6.Text = "Skip";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem5.Text = "Start";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // button1true
            // 
            this.button1true.BackColor = System.Drawing.Color.GreenYellow;
            this.button1true.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1true.Location = new System.Drawing.Point(24, 124);
            this.button1true.Name = "button1true";
            this.button1true.Size = new System.Drawing.Size(75, 23);
            this.button1true.TabIndex = 17;
            this.button1true.Text = "True";
            this.button1true.UseVisualStyleBackColor = false;
            this.button1true.Visible = false;
            this.button1true.Click += new System.EventHandler(this.button1true_Click_1);
            // 
            // button2false
            // 
            this.button2false.BackColor = System.Drawing.Color.LightCoral;
            this.button2false.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2false.Location = new System.Drawing.Point(105, 124);
            this.button2false.Name = "button2false";
            this.button2false.Size = new System.Drawing.Size(75, 23);
            this.button2false.TabIndex = 18;
            this.button2false.Text = "False";
            this.button2false.UseVisualStyleBackColor = false;
            this.button2false.Visible = false;
            this.button2false.Click += new System.EventHandler(this.button2false_Click_1);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(300, 227);
            this.Controls.Add(this.button2false);
            this.Controls.Add(this.button1true);
            this.Controls.Add(this.pbCaptchaImage);
            this.Controls.Add(this.txtCaptcha);
            this.Controls.Add(this.sttInfo);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuTop;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(316, 265);
            this.Name = "FrmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "9kwGUI   Version 1.35";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuTop.ResumeLayout(false);
            this.menuTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaptchaImage)).EndInit();
            this.sttInfo.ResumeLayout(false);
            this.sttInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuTray.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCredits;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnSkip;
        private System.Windows.Forms.MenuStrip menuTop;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbCaptchaImage;
        private System.Windows.Forms.TextBox txtCaptcha;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.StatusStrip sttInfo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblInQueue;
        private System.Windows.Forms.Label lblnWork;
        private System.ComponentModel.BackgroundWorker bgwGetCaptchaID;
        private System.ComponentModel.BackgroundWorker bgwUpdateCredits;
        private System.ComponentModel.BackgroundWorker bgwUpdateServerStatus;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip menuTray;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.Button button1true;
        private System.Windows.Forms.Button button2false;
        private System.Windows.Forms.Label label2;
    }
}

