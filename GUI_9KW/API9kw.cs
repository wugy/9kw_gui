﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;


namespace GUI_9KW
{
    /// <summary>
    /// @author Eisfuchs
    /// Basic functions to work with the API
    /// </summary>
    public class API9kw
    {
        public string url { get; set; }
        public string captcha { get; set; }
        public string apikey { get; set; }
        private RequestType rType;
        
        public API9kw() 
        {
            this.url = "http://www.9kw.eu/index.cgi?";
            this.apikey = "&apikey=" + Properties.Settings.Default.apikey;
            rType = new RequestType();
        }

        /// <summary>
        /// Function to get the data from the server
        /// </summary>
        /// <param name="type">Type of which should be made</param>
        /// <returns>Content from the Requesttype</returns>
        private string GetRequestData(string type)
        {
            apikey = "&apikey=" + Properties.Settings.Default.apikey;
            WebRequest wrequest = WebRequest.Create(url + type + apikey);
            if (Properties.Settings.Default.Proxy == false)
            {
                // Making the request much faster then before
                wrequest.Proxy = null;
            }
            else
            {
                wrequest.Proxy = WebRequest.GetSystemWebProxy();
            }
            Stream responsestream = wrequest.GetResponse().GetResponseStream();
            StreamReader streamreader = new StreamReader(responsestream, Encoding.UTF8);
            string content = streamreader.ReadToEnd();
            streamreader.Close();
            responsestream.Close();
            return content;
        }

        /// <summary>
        /// Returns the Image from a captchaID
        /// </summary>
        /// <param name="captchaID">The ID of the captcha which should be solved</param>
        /// <returns>Image</returns>
        public System.Drawing.Image GetImage(string captchaID, bool fast, bool confirmthing)
        {
            WebRequest wrequest;
            if(fast == true && confirmthing == false){// Fast API
                wrequest = WebRequest.Create("http://www.9kw.eu/grafik/captchas/" + captchaID + ".txt");
            }else{// Slow API
                if (Properties.Settings.Default.Speed && confirmthing == false)
                {
                    wrequest = WebRequest.Create(url + new RequestType().showCaptcha + "&speed=1&id=" + captchaID + apikey);
                }else{
                    wrequest = WebRequest.Create(url + new RequestType().showCaptcha + "&id=" + captchaID + apikey);
                }
            }
            if (Properties.Settings.Default.Proxy == false)
            {
                // Making the request much faster then before
                wrequest.Proxy = null;
            }
            else 
            {
                wrequest.Proxy = WebRequest.GetSystemWebProxy();
            }
            Stream responsestream = wrequest.GetResponse().GetResponseStream();
            System.Drawing.Image img = System.Drawing.Image.FromStream(responsestream);
            responsestream.Close();
            return img;
        }

        /// <summary>
        /// Invoke the request function to get the new server stats
        /// </summary>
        /// <returns></returns>
        public string GetServerStatus()
        {
            return GetRequestData(rType.serverStatus);
        }

        /// <summary>
        /// Getting a new CaptchaID
        /// </summary>
        /// <returns></returns>
        public string GetCaptchaID()
        {
            String dataSet = "";
            if (Properties.Settings.Default.Text)
            {
                dataSet = dataSet + "&text=yes";
            }
            else 
            {
                dataSet = dataSet + "&text=no";
            }

            if (Properties.Settings.Default.Mouse)
            {
                dataSet = dataSet + "&mouse=1";
            }
            else 
            {
                dataSet = dataSet + "&mouse=0";
            }

            if (Properties.Settings.Default.Confirm)
            {
                dataSet = dataSet + "&confirm=1";
            }
            else 
            {
                dataSet = dataSet + "&confirm=0";
            }
            return GetRequestData(rType.captchaID + dataSet);
        }

        /// <summary>
        /// Send back the solution
        /// </summary>
        /// <param name="solution"></param>
        /// <param name="captchaID"></param>
        /// <returns></returns>
        public string SendResolvedCaptcha(string solution, string captchaID)
        {
            String newCaptchaID;
            Match match = Regex.Match(captchaID, @"^(\d+)");
            if (match.Success)
            {
                newCaptchaID = match.Groups[1].Value.ToString();
            }
            else
            {
                newCaptchaID = captchaID;
            }

            Match matchconfirm = Regex.Match(captchaID, @"^\d+\|\w+\|confirm\|([^\|]+)\|");
            if (matchconfirm.Success)
            {
                return GetRequestData(rType.captcha + "&id=" + newCaptchaID + "&confirm=1&captcha=" + System.Uri.EscapeDataString(solution));
            }
            else
            {
                return GetRequestData(rType.captcha + "&id=" + newCaptchaID + "&captcha=" + System.Uri.EscapeDataString(solution));
            }
        }

        /// <summary>
        /// Skipping a captcha
        /// </summary>
        /// <param name="captchaID"></param>
        /// <returns></returns>
        public string SkipCaptcha(string captchaID)
        {
            return GetRequestData(rType.skipCaptcha + "&id=" + captchaID);
        }

        /// <summary>
        /// Reads and validates the Credits from the response stream
        /// </summary>
        /// <returns>Credits from the user</returns>
        public string GetCredits()
        {
            String data = GetRequestData((new RequestType()).captchaStatus);
            Regex myRegex = new Regex("^[0-9]*$");
            Regex myRegex2 = new Regex("^[0-9]+ [A-Za-z0-9]+");
            if (myRegex.IsMatch(data) || myRegex2.IsMatch(data))
            {
                return data;
            }
            else
            {
                return "failed";
            }
        }
    }
}
