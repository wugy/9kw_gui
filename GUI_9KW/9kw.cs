﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace GUI_9KW
{
    class _9kw
    {
        private String apiurl = "http://www.9kw.eu/index.cgi?";
        private String CaptchaID = "";
        private volatile bool _shouldStop;

        #region Delegates
        // Gekapseltes Delegate
        private EventHandler _Update_MainGui_Credits;
        // Definition des Events
        public event EventHandler Update_MainGui_Credits
        {
            add
            {
                _Update_MainGui_Credits += value;
            }
            remove
            {
                _Update_MainGui_Credits -= value;
            }
        }

        private EventHandler _Update_MainGui_User;
        public event EventHandler Update_MainGui_User
        {
            add
            {
                _Update_MainGui_User += value;
            }
            remove
            {
                _Update_MainGui_User -= value;
            }
        }

        private EventHandler _Update_MainGui_InWork;
        public event EventHandler Update_MainGui_InWork
        {
            add
            {
                _Update_MainGui_InWork += value;
            }
            remove
            {
                _Update_MainGui_InWork -= value;
            }
        }

        private EventHandler _Update_MainGui_InQueue;
        public event EventHandler Update_MainGui_InQueue
        {
            add
            {
                _Update_MainGui_InQueue += value;
            }
            remove
            {
                _Update_MainGui_InQueue -= value;
            }
        }

        private EventHandler _Update_MainGui_statusStrip1;
        public event EventHandler Update_MainGui_statusStrip1
        {
            add
            {
                _Update_MainGui_statusStrip1 += value;
            }
            remove
            {
                _Update_MainGui_statusStrip1 -= value;
            }
        }

        #endregion


        private API9kw api = new API9kw();

        public void RequestStart()
        {

            _shouldStop = false;

        }
        public void RequestStop()
        {
            
                _shouldStop = true;
                      
        }

        public void UpdateCredits()
        {
            while (!_shouldStop)
            {
                try
                {
                    _Update_MainGui_statusStrip1("Updating Credits...", new EventArgs());
                    string credits = api.GetCredits();
                    if (credits != "failed")
                    {
                        _Update_MainGui_Credits(credits, new EventArgs());
                        _Update_MainGui_statusStrip1("Credits updated.", new EventArgs());
                    }
                    else 
                    {
                        _Update_MainGui_statusStrip1("Credits update failed." + credits, new EventArgs());
                    }
                    System.Threading.Thread.Sleep(45000);
                }
                catch
                {
                    RequestStop();
                    _Update_MainGui_statusStrip1("Credits update failed.", new EventArgs());
                    System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");
                }
            }
        }

        public void UpdateServerStatus()
        {

            String rqurl = apiurl + "action=userservercheck&source=9kwgui";

            while (!_shouldStop)
            {
                try
                {
                    _Update_MainGui_statusStrip1("Updating Server Status...", new EventArgs());

                    string status = api.GetServerStatus();

                    Match match = Regex.Match(status, @"(?<=worker=)\d{1,6}");
                    if (match.Success)
                    {
                        _Update_MainGui_User(match, new EventArgs());
                    }

                    match = Regex.Match(status, @"(?<=inwork=)\d{1,6}");
                    if (match.Success)
                    {
                        _Update_MainGui_InWork(match, new EventArgs());
                    }

                    match = Regex.Match(status, @"(?<=queue=)\d{1,6}");
                    if (match.Success)
                    {
                        _Update_MainGui_InQueue(match, new EventArgs());
                    }

                    _Update_MainGui_statusStrip1("Server Status updated.", new EventArgs());
                    System.Threading.Thread.Sleep(100000);
                }
                catch
                {
                    RequestStop();
                    _Update_MainGui_statusStrip1("Server Status update failed.", new EventArgs());
                    System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");

                }
               /* finally 
                {
                    System.Threading.Thread.Sleep(300000);
                }
                */
                
            }
        }

        public String GetCaptchaID()
        {
            CaptchaID = "";
            String StopString = "Stop";
            if (Properties.Settings.Default.apikey.Length != 0)
            {
                while (!_shouldStop)
                {
                    while (CaptchaID == "" & _shouldStop == false)
                    {
                        try
                        {
                            _Update_MainGui_statusStrip1("Trying to get Captcha ID...", new EventArgs());
                            CaptchaID = api.GetCaptchaID();

                        }
                        catch (WebException)
                        {
                            RequestStop();
                            _Update_MainGui_statusStrip1("Failed to get Captcha ID.", new EventArgs());
                            System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");

                        }

                        if (CaptchaID != "")
                        {
                            Console.WriteLine(CaptchaID);
                            if (_shouldStop)
                            {
                                SkipCaptcha();
                                
                                return StopString;
                            }
                            else
                            {
                                Regex myRegex2 = new Regex("^[0-9]+ [A-Za-z0-9]+");
                                if (myRegex2.IsMatch(CaptchaID))
                                {
                                    _Update_MainGui_statusStrip1(CaptchaID, new EventArgs());
                                    return CaptchaID;
                                }
                                else {
                                    _Update_MainGui_statusStrip1("Got Captcha ID = " + CaptchaID, new EventArgs());
                                    return CaptchaID;
                                }
                            }
                        }

                        _Update_MainGui_statusStrip1("No new Captcha. Waiting 1 second", new EventArgs());
                        System.Threading.Thread.Sleep(1000);

                    }
                }
            }
            StopString = "Stop";
            return StopString;
        }

        public void ReturnSolvedCaptcha(String response)
        {
            try
            {
                String rqurl = apiurl + "action=usercaptchacorrect&source=9kwgui&id=" + CaptchaID + "&captcha=" + System.Uri.EscapeDataString(response) + "&apikey=" + Properties.Settings.Default.apikey;
                
                string result = api.SendResolvedCaptcha(response, CaptchaID);

                if (result == "OK")
                {
                    _Update_MainGui_statusStrip1("Your answer was sent...", new EventArgs());
                }
                else
                {
                    _Update_MainGui_statusStrip1("Your answer couldnt be sent..." + result, new EventArgs());
                }
                CaptchaID = "";
            }
            catch (WebException)
            {
                RequestStop();
                _Update_MainGui_statusStrip1("Failed to send answer. No connection", new EventArgs());
                System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");

            }
        }

        public void SkipCaptcha()
        {
            
            if (CaptchaID != "")
            {
                try
                {
                    string result = api.SkipCaptcha(CaptchaID);
                    if (result == "OK" & _shouldStop == true)
                    {
                        _Update_MainGui_statusStrip1("Skipped current CaptchaID and Stopped.", new EventArgs());
                    }

                    else if (result == "OK")
                    {
                        _Update_MainGui_statusStrip1("Captcha skipped...", new EventArgs());
                    }

                    else
                    {
                        _Update_MainGui_statusStrip1("Error while skipping...", new EventArgs());
                    }
                    CaptchaID = "";
                }
                catch (WebException)
                {
                    RequestStop();
                    _Update_MainGui_statusStrip1("Error while skipping. No connection", new EventArgs());
                    System.Windows.Forms.MessageBox.Show("Couldn't connect to the API Server.\r\nCheck your internet connection (Firewall?).\r\nIf its ok it seems the API Server is not responding.");

                }
            }
        }



    }
}
