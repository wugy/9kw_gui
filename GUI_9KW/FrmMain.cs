﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;

namespace GUI_9KW
{
    public partial class FrmMain : Form
    {
        private bool _MouseActiv = false;

        public FrmMain()
        {
            InitializeComponent();
        }

        delegate void SetTextCallback(object sender, EventArgs e);
        //private bool reallyexit = false;
        
        private void Update_MainGui_Credits(object sender, EventArgs e)
        {   
            if (this.lblCredits.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Update_MainGui_Credits);
                this.Invoke(d, new object[] { sender.ToString(), e });
            }
            else
            {
                this.lblCredits.Text = sender.ToString();
            }
        }

        private void Update_MainGui_User(object sender, EventArgs e)
        {
            if (this.lblUser.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Update_MainGui_User);
                this.Invoke(d, new object[] { sender.ToString(), e });
            }
            else
            {
                this.lblUser.Text = sender.ToString();
            }
        }

        private void Update_MainGui_InWork(object sender, EventArgs e)
        {
            if (this.lblnWork.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Update_MainGui_InWork);
                this.Invoke(d, new object[] { sender.ToString(), e });
            }
            else
            {
                this.lblnWork.Text = sender.ToString();
            }
        }

        private void Update_MainGui_InQueue(object sender, EventArgs e)
        {
            if (this.lblInQueue.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Update_MainGui_InQueue);
                this.Invoke(d, new object[] { sender.ToString(), e });
            }
            else
            {
                this.lblInQueue.Text = sender.ToString();
            }
        }

        private void Update_MainGui_statusStrip1(object sender, EventArgs e)
        {
            if (this.sttInfo.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Update_MainGui_statusStrip1);
                this.Invoke(d, new object[] { sender.ToString(),e });
            }
            else
            {
                this.toolStripStatusLabel1.Text = sender.ToString();
            }
        }

        private _9kw my9kwclass = new _9kw();

        private void startstopProgramm()
        {
            if (btnStart.Text == "Start")
            {
                if (Properties.Settings.Default.apikey == "")
                {
                    System.Windows.Forms.MessageBox.Show("Please enter an APIKEY in Settings");
                }
                else
                {
                    my9kwclass.RequestStart();

                    if (!bgwUpdateCredits.IsBusy)
                    {
                        bgwUpdateCredits.RunWorkerAsync();
                    }
                    if (!bgwGetCaptchaID.IsBusy)
                    {
                        bgwGetCaptchaID.RunWorkerAsync();
                    }
                    if (!bgwUpdateServerStatus.IsBusy)
                    {
                        bgwUpdateServerStatus.RunWorkerAsync();
                    }
                }
                btnStart.Text = "Stop";
                toolStripMenuItem5.Text = "Stop";
                this.Icon = Properties.Resources.On;
                notifyIcon1.Icon = Properties.Resources.On;
            }
            else if (btnStart.Text == "Stop")
            {
                timer1.Stop();

                my9kwclass.RequestStop();
                if (pbCaptchaImage.Image != null)
                {
                    my9kwclass.SkipCaptcha();
                }
                bgwUpdateCredits.CancelAsync();
                bgwGetCaptchaID.CancelAsync();
                bgwUpdateServerStatus.CancelAsync();
                this.toolStripStatusLabel1.Text = "Stopped!";
                pbCaptchaImage.Image = null;
                this._MouseActiv = false;
                txtCaptcha.Visible = true;
                button1true.Visible = false;
                button2false.Visible = false;
                label2.Visible = false;

                if (!Properties.Settings.Default.ZoomManually)
                {
                    if (Properties.Settings.Default.HideElements)
                    {
                        this.Size = new Size(316, 118);
                    }
                    else
                    {
                        this.Size = new Size(316, 265);
                    }
                }

                lblCounter.Text = "29";
                txtCaptcha.Text = "";
                btnStart.Text = "Start";
                toolStripMenuItem5.Text = "Start";
                this.Icon = Properties.Resources.Off;
                notifyIcon1.Icon = Properties.Resources.Off;
            }
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            startstopProgramm();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (pbCaptchaImage.Image != null)
            {
                timer1.Stop();
                lblCounter.Text = "29";
                my9kwclass.SkipCaptcha();
                pbCaptchaImage.Image = null;
                if (!Properties.Settings.Default.ZoomManually)
                {
                    if (Properties.Settings.Default.HideElements)
                    {
                        this.Size = new Size(316, 118);
                    }
                    else
                    {
                        this.Size = new Size(316, 265);
                    }
                }
                if (Properties.Settings.Default.AutoMinimize)
                {
                    toolStripMenuItem2.PerformClick();

                }   
                txtCaptcha.Text = "";
                if (btnStart.Text == "Stop")
                {
                    bgwGetCaptchaID.RunWorkerAsync();
                }
            }
        }

        private void bgwGetCaptchaID_DoWork(object sender, DoWorkEventArgs e)
        {
            
            e.Result = my9kwclass.GetCaptchaID();
        }

        private void bgwGetCaptchaID_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // muss man noch schoener machen, mit reg die id rausholen
            Regex myRegex2 = new Regex("^[0-9]+ [A-Za-z0-9]+");
            if (e.Result.ToString() != "Stop" && !myRegex2.IsMatch(e.Result.ToString()))
            {
                bool normalget = true;
                bool confirmthing = false;
                String newAnswer;
                txtCaptcha.Text = "";

                Match matchconfirm = Regex.Match(e.Result.ToString(), @"^\d+\|\w+\|confirm\|([^\|]+)\|");
                if (matchconfirm.Success)
                {
                    newAnswer = matchconfirm.Groups[1].Value.ToString();
                    confirmthing = true;
                    Update_MainGui_statusStrip1("SOLUTION: " + newAnswer, new EventArgs());
                    txtCaptcha.Visible = false;
                    button1true.Visible = true;
                    button2false.Visible = true;
                    label2.Text = "SOLUTION: " + newAnswer;
                    label2.Visible = true;
                }
                else 
                {
                    txtCaptcha.Visible = true;
                    button1true.Visible = false;
                    button2false.Visible = false;
                    label2.Visible = false;
                }

                String newCaptchaID;
                Match match = Regex.Match(e.Result.ToString(), @"^(\d+)");
                if (match.Success)
                {
                    newCaptchaID = match.Groups[1].Value.ToString();
                }
                else
                {
                    newCaptchaID = e.Result.ToString();
                }

                if (Properties.Settings.Default.Speed)
                {
                    normalget = false;
                }

                API9kw api = new API9kw();
                System.Drawing.Image image = api.GetImage(newCaptchaID, normalget, confirmthing);
                if (Properties.Settings.Default.Grayscale)
                {
                    image = CreateGrayscaleImage(image);
                }

                pbCaptchaImage.Image = image;
                this.WindowState = FormWindowState.Normal;
                timer1.Interval = 1000;
                if (Properties.Settings.Default.HideElements)
                {
                    this.Text = "29";   
                }
                else 
                {
                    lblCounter.Text = "29";
                }
                timer1.Start();
                if (!this.Visible && Properties.Settings.Default.AutoMinimize)
                {
                    this.Show();
                    toolStripMenuItem2.Text = "Hide GUI window";
                }
                if (Properties.Settings.Default.ActivateWindow)
                {
                    this.Activate();
                }
                txtCaptcha.Focus();

                // Kein Zoom für Mouse Captchas oder Berechnung anpassen!
                Regex Regexmouse = new Regex(@"^\d+\|mouse$");
                if (Regexmouse.IsMatch(e.Result.ToString()) && confirmthing == false)
                {
                    this._MouseActiv = true;
                    txtCaptcha.Visible = false;
                    pbCaptchaImage.SizeMode = PictureBoxSizeMode.Normal;
                    this.SizeGripStyle = SizeGripStyle.Hide;

                    if (Properties.Settings.Default.HideElements)
                    {
                        this.Size = new Size(image.Width + 16, image.Height + 58);
                    }
                    else
                    {
                        this.Size = new Size(image.Width + 16, image.Height + 205);
                    }
                }
                else
                {
                    txtCaptcha.Visible = true;
                    if (Properties.Settings.Default.ZoomManually)
                    {
                        pbCaptchaImage.SizeMode = PictureBoxSizeMode.Zoom;
                        this.SizeGripStyle = SizeGripStyle.Show;
                    }
                    else
                    {
                        pbCaptchaImage.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.SizeGripStyle = SizeGripStyle.Hide;
                    }

                    if (!Properties.Settings.Default.ZoomManually)
                    {
                        if (Properties.Settings.Default.HideElements)
                        {
                            this.Size = new Size((image.Width * Properties.Settings.Default.Zoom / 100) + 16, (image.Height * Properties.Settings.Default.Zoom / 100) + 58);
                        }
                        else
                        {
                            this.Size = new Size((image.Width * Properties.Settings.Default.Zoom / 100) + 16, (image.Height * Properties.Settings.Default.Zoom / 100) + 205);
                            //this.Size = new Size((image.Width * Properties.Settings.Default.Zoom / 100) + 16, (image.Height * Properties.Settings.Default.Zoom / 100) + 105);
                        }
                    }
                }       

                try
                {
                    if (Properties.Settings.Default.NoSound)
                    {
                        // No sound
                    }else{
                        if (Properties.Settings.Default.UseWindowsSound)
                        {
                            System.Media.SystemSounds.Beep.Play();
                        }
                        else if (Properties.Settings.Default.UseWaveSound)
                        {
                            System.Media.SoundPlayer startSoundPlayer = new System.Media.SoundPlayer(Properties.Settings.Default.WaveSoundFolder);
                            startSoundPlayer.Play();
                        }
                        else if (Properties.Settings.Default.UseInternalSound)
                        {
                            System.Media.SoundPlayer startSoundPlayer = new System.Media.SoundPlayer(Properties.Resources.alert);
                            startSoundPlayer.Play();
                        }
                    }
                }
                catch
                { 
                
                }
 
            }
            else
            {
                if(myRegex2.IsMatch(e.Result.ToString())){
                    Update_MainGui_statusStrip1(e.Result.ToString(), new EventArgs());
                    bgwUpdateCredits.CancelAsync();
                    bgwGetCaptchaID.CancelAsync();

                    lblCounter.Text = "29";
                    txtCaptcha.Text = "";
                    btnStart.Text = "Start";
                    toolStripMenuItem5.Text = "Start";
                    this.Icon = Properties.Resources.Off;
                    notifyIcon1.Icon = Properties.Resources.Off;
                }else{
                    Update_MainGui_statusStrip1("Stopped!", new EventArgs());
                }
            }
            
            
        }

        private Image CreateGrayscaleImage(Image image)
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            Graphics g = Graphics.FromImage(newBitmap);
            ColorMatrix colorMatrix = new ColorMatrix(

            new float[][] 
            {
                new float[] {.3f, .3f, .3f, 0, 0},
                new float[] {.59f, .59f, .59f, 0, 0},
                new float[] {.11f, .11f, .11f, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });

            ImageAttributes attributes = new ImageAttributes();
            attributes.SetColorMatrix(colorMatrix);
            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);

            g.Dispose();
            return newBitmap;
        }

        // dieser Code wird ausgeführt, wenn der Timer abgelaufen ist
        void timer1_End(object sender, EventArgs e)   
        {
           
            if (lblCounter.Text == "1" || this.Text == "1")
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                    
                }
                txtCaptcha.Visible = true;
                button1true.Visible = false;
                button2false.Visible = false;
                label2.Visible = false;
                btnSkip.PerformClick();
                btnStart.Text = "Stop";
                my9kwclass.RequestStop();
            }
            else
            {
                if (Properties.Settings.Default.HideElements)
                {
                    this.Text = (Convert.ToInt32(this.Text) - 1).ToString();
                }
                else
                {
                    lblCounter.Text = (Convert.ToInt32(lblCounter.Text) - 1).ToString();
                }
            }
        }

        private void bgwUpdateCredits_DoWork(object sender, DoWorkEventArgs e)
        {
            my9kwclass.UpdateCredits();        
        }

        private void bgwUpdateServerStatus_DoWork(object sender, DoWorkEventArgs e)
        {
            my9kwclass.UpdateServerStatus();
        }
        
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "Stop")
            {
                btnStart.PerformClick();
            }
            SettingsForm settingsform = new SettingsForm();
            settingsform.StartPosition = FormStartPosition.CenterParent;
            if (settingsform.ShowDialog(this) == DialogResult.OK)
            {
                if (Properties.Settings.Default.ZoomManually)
                {
                    this.SizeGripStyle = SizeGripStyle.Show;
                    pbCaptchaImage.SizeMode = PictureBoxSizeMode.Zoom;
                }
                else
                {
                    pbCaptchaImage.SizeMode = PictureBoxSizeMode.StretchImage;
                    this.SizeGripStyle = SizeGripStyle.Hide;
                    this.Size = new Size(316, 265);
                }

                if (Properties.Settings.Default.HideElements)
                {
                    panel1.Size = new Size(0, 0);
                    menuTop.Size = new Size(0, 0);
                    sttInfo.Size = new Size(0, 0);

                    this.MinimumSize = new Size(316, 118);
                    this.Size = new Size(316, 118);
                    pbCaptchaImage.Location = new Point(0, 0);
                }
                else
                {
                    panel1.Size = new Size(300, 100);
                    menuTop.Size = new Size(300, 24);
                    sttInfo.Size = new Size(300, 23);
                    this.Text = "9kwGUI   Version 1.35";
                    this.MinimumSize = new Size(316, 265);
                    this.Size = new Size(316, 265);
                }

            } 
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtCaptcha.Text != "")
            {
                this.CompleteCaptcha();
                /*
                if (Convert.ToInt32(label3.Text) < 28)
                {
                    if (Properties.Settings.Default.AutoMinimize)
                    {
                        toolStripMenuItem2.PerformClick();

                    }
                    e.SuppressKeyPress = true;
                    pictureBox1.Image = null;
                    if (!Properties.Settings.Default.ZoomManually)
                    {
                        if (Properties.Settings.Default.HideElements)
                            this.Size = new Size(316, 118);
                        else
                            this.Size = new Size(316, 265);


                    }
                    timer1.Stop();
                    my9kwclass.ReturnSolvedCaptcha(textBox1.Text.ToString());
                    bgwGetCaptchaID.RunWorkerAsync();


                    label3.Text = "29";


                    textBox1.Text = "";
                }
                
                if (Properties.Settings.Default.HideElements)
                {
                    if (Convert.ToInt32(this.Text) < 28)
                    {
                        if (Properties.Settings.Default.AutoMinimize)
                        {
                            toolStripMenuItem2.PerformClick();

                        }
                        e.SuppressKeyPress = true;
                        pictureBox1.Image = null;
                        if (!Properties.Settings.Default.ZoomManually)
                        {
                            
                                this.Size = new Size(316, 118);
                            

                        }
                        timer1.Stop();

                        my9kwclass.ReturnSolvedCaptcha(textBox1.Text.ToString());
                        bgwGetCaptchaID.RunWorkerAsync();

                        
                            this.Text = "29";
                        

                        textBox1.Text = "";
                    }


                }
                else
                {
                    if (Convert.ToInt32(label3.Text) < 28)
                    {
                        if (Properties.Settings.Default.AutoMinimize)
                        {
                            toolStripMenuItem2.PerformClick();

                        }
                        e.SuppressKeyPress = true;
                        pictureBox1.Image = null;
                        if (!Properties.Settings.Default.ZoomManually)
                        {
                            
                                this.Size = new Size(316, 265);
                            

                        }
                        timer1.Stop();
                        my9kwclass.ReturnSolvedCaptcha(textBox1.Text.ToString());
                        bgwGetCaptchaID.RunWorkerAsync();

                        
                            label3.Text = "29";
                        

                        textBox1.Text = "";
                    }    
                }*/
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.apikey == "")
            {
               
                settingsToolStripMenuItem.PerformClick();
                
            }
            this.toolStripStatusLabel1.Text = "Stopped";
            my9kwclass.Update_MainGui_Credits += new EventHandler(Update_MainGui_Credits);
            my9kwclass.Update_MainGui_statusStrip1 += new EventHandler(Update_MainGui_statusStrip1);
            my9kwclass.Update_MainGui_User += new EventHandler(Update_MainGui_User);
            my9kwclass.Update_MainGui_InWork += new EventHandler(Update_MainGui_InWork);
            my9kwclass.Update_MainGui_InQueue += new EventHandler(Update_MainGui_InQueue);
            timer1.Tick += new EventHandler(timer1_End);
            bgwUpdateServerStatus.RunWorkerAsync();
            bgwUpdateCredits.RunWorkerAsync(); 

            if (Properties.Settings.Default.AutoStart)
            {
                btnStart.PerformClick();
                if (Properties.Settings.Default.AutoMinimize)
                {
                    
                        BeginInvoke(new MethodInvoker(delegate
                        {
                            Hide();
                        }));
                    
                    toolStripMenuItem2.PerformClick();    
                }
                

            }
            
            if (Properties.Settings.Default.ZoomManually)
            {
                this.SizeGripStyle = SizeGripStyle.Show;
                pbCaptchaImage.SizeMode = PictureBoxSizeMode.Zoom;
            }
            

            if (Properties.Settings.Default.HideElements)
            {
                panel1.Size = new Size(0, 0);
                menuTop.Size = new Size(0, 0);
                sttInfo.Size = new Size(0, 0);

                this.MinimumSize = new Size(316, 118);
                this.Size = new Size(316, 118);
                pbCaptchaImage.Location = new Point(0, 0);
            }
            this.DesktopBounds = Properties.Settings.Default.WindowPosition;            
        }

        private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
        {

            // X only to tray (activate reallyexit and this)
            //if (e.CloseReason == CloseReason.UserClosing & reallyexit == false)
            //{

            //    e.Cancel = true;
            //    toolStripMenuItem2.PerformClick();

            //}
            //else
            //{
                this.WindowState = FormWindowState.Normal;
                Properties.Settings.Default.WindowPosition = this.DesktopBounds;
                Properties.Settings.Default.Save();
            //}

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm aboutform = new AboutForm(); 
            aboutform.StartPosition = FormStartPosition.CenterParent;
            aboutform.ShowDialog(this);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Hide();
                toolStripMenuItem2.Text = "Show GUI";
            }
            else if (!this.Visible)
            {
                
                this.Show();
                this.Activate();        
                toolStripMenuItem2.Text = "Hide GUI";
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                toolStripMenuItem2.PerformClick();
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            //reallyexit = true;
            this.Close();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            settingsToolStripMenuItem.PerformClick();
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                
                    this.WindowState = FormWindowState.Normal;
                    toolStripMenuItem2.PerformClick();
                    
            }
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {   
            
            startstopProgramm();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnSkip.PerformClick();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left){
                if (this._MouseActiv)
                {
                    txtCaptcha.Text = e.Location.X + "x" + e.Location.Y;
                    //For tests:
                    //MessageBox.Show(string.Format("X: {0} Y: {1}", e.Location.X, e.Location.Y));
                    this.CompleteCaptcha();
                    txtCaptcha.Visible = true;
                }
            }

        }

        private void CompleteCaptcha()
        {
            if (Convert.ToInt32(lblCounter.Text) < 28 || Convert.ToInt32(this.Text) < 28)
            {
                if (Properties.Settings.Default.AutoMinimize)
                {
                    toolStripMenuItem2.PerformClick();

                }
                //e.SuppressKeyPress = true;
                pbCaptchaImage.Image = null;
                if (!Properties.Settings.Default.ZoomManually)
                {
                    if (Properties.Settings.Default.HideElements)
                        this.Size = new Size(316, 118);
                    else
                        this.Size = new Size(316, 265);


                }
                timer1.Stop();
                my9kwclass.ReturnSolvedCaptcha(txtCaptcha.Text.ToString());
                bgwGetCaptchaID.RunWorkerAsync();

                if (!Properties.Settings.Default.HideElements)
                    lblCounter.Text = "29";
                else
                    this.Text = "29";


                txtCaptcha.Text = "";
            }
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            if (this._MouseActiv)
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            if (this._MouseActiv)
            {
                this.Cursor = Cursors.Cross;
            }
        }

        private void button1true_Click_1(object sender, EventArgs e)
        {
            txtCaptcha.Text = "yes";
            this.CompleteCaptcha();
            txtCaptcha.Visible = true;
            button1true.Visible = false;
            button2false.Visible = false;
            label2.Visible = false;
        }

        private void button2false_Click_1(object sender, EventArgs e)
        {
            txtCaptcha.Text = "no";
            this.CompleteCaptcha();
            txtCaptcha.Visible = true;
            button1true.Visible = false;
            button2false.Visible = false;
            label2.Visible = false;
        }
    }
}
